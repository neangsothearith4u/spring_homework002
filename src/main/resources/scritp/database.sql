create table product_tb
(
    product_id    serial primary key,
    product_name  varchar(100),
    product_price numeric(7, 2)
);

create table customer_tb
(
    customer_id      serial primary key,
    customer_name    varchar(100),
    customer_address varchar(100),
    customer_phone   int4
);

create table invoice_tb
(
    invoice_id   serial primary key,
    invoice_date timestamp default now(),
    customer_id  int not null,

    foreign key (customer_id) references customer_tb (customer_id) on delete cascade on update cascade
);

create table invoice_detail_db
(
    id         serial primary key,
    invoice_id int,
    product_id int,

    FOREIGN KEY (invoice_id) references invoice_tb (invoice_id) on delete cascade on update cascade,
    FOREIGN KEY (product_id) References product_tb (product_id) on delete cascade on update cascade
);






-- manipulation
SELECT * FROM customer_tb where customer_id = 1;

INSERT INTO customer_tb (customer_name, customer_address, customer_phone) values ('Theary','TK',9865432) returning *;

UPDATE customer_tb SET customer_name = 'Theara',customer_address='TK',customer_phone = 123456 where customer_id = 4;

DELETE FROM customer_tb where customer_id = 1;



--  product
SELECT * FROM product_tb;
INSERT INTO product_tb (product_name, product_price) VALUES ('Pepsi',1.5)  ;
UPDATE product_tb SET product_name = 'Heneiken',product_price=2.5 where product_id=5 returning *;


-- Invoice
Insert Into invoice_tb (customer_id) values () returning invoice_id;

INSERT INTO invoice_detail_db (invoice_id, product_id) VALUES ();