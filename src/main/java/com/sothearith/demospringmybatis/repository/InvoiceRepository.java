package com.sothearith.demospringmybatis.repository;

import com.sothearith.demospringmybatis.model.entity.Invoice;
import com.sothearith.demospringmybatis.model.request.InvoiceRequest;
import com.sothearith.demospringmybatis.service.service.InvoiceService;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {


    // Select All invoice
    @Select("""
            SELECT * FROM invoice_tb
            """)
    @Result(property = "customerId",column = "customer_id",
        one = @One(select = "com.sothearith.demospringmybatis.repository.CustomerRepository.getCustomerById")
    )
    @Result(property = "products", column = "invoice_id",
    many = @Many(select = "com.sothearith.demospringmybatis.repository.ProductRepository.getAllProductByInvoiceId")
    )
    @Result(property = "invoiceId",column = "invoice_id")
    @Result(property = "invoiceDate",column = "invoice_date")
    List<Invoice> getAllInvoice();



    // Select by ID
    @Select("""
            SELECT * FROM invoice_tb where invoice_id = #{id}
            """)


    @Result(property = "customerId",column = "customer_id",
            one = @One(select = "com.sothearith.demospringmybatis.repository.CustomerRepository.getCustomerById")
    )
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.sothearith.demospringmybatis.repository.ProductRepository.getAllProductByInvoiceId")
    )
    @Result(property = "invoiceId",column = "invoice_id")
    @Result(property = "invoiceDate",column = "invoice_date")
    Invoice getInvoiceById(Integer id);


    // Add new invoice
    @Select("""
            Insert Into invoice_tb (customer_id) values (#{invoice.customerId}) returning invoice_id
            """)
    @Result(property = "customerId",column = "invoice_id")
    Integer addNewInvoice(@Param("invoice") InvoiceRequest invoiceRequest);

    @Insert("""
            INSERT INTO invoice_detail_db (invoice_id, product_id) VALUES (#{invoiceId},#{productId})
            """)
    void insertIntoInvoiceDetail(Integer invoiceId,Integer productId);


    @Select("""
            UPDATE invoice_tb set customer_id =#{invoice.customerId} where invoice_id = #{id} returning invoice_id
            """)
    void updateInvoice(Integer id,@Param("invoice") InvoiceService invoiceService);

    // Update Invoice
    @Insert("""
            delete from  invoice_detail_db where invoice_id = #{invoiceId}
            """)
    void deleteInvoiceDetail(Integer invoiceId);




}
