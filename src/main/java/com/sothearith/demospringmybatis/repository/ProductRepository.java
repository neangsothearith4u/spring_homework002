package com.sothearith.demospringmybatis.repository;

import com.sothearith.demospringmybatis.model.entity.Product;
import com.sothearith.demospringmybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("""
            SELECT * FROM product_tb
            """)
    @Results(id = "resultMapping",value = {
            @Result(property = "productId",column = "product_id"),
            @Result(property = "productName",column = "product_name"),
            @Result(property = "productPrice",column = "product_price")
    })
    List<Product> getAllProduct();

    @Select("""
            SELECT * FROM product_tb where product_id = #{id}
            """)
    @ResultMap("resultMapping")
    Product getProductById(Integer id);

    @Select("""
            INSERT INTO product_tb (product_name, product_price) VALUES (#{product.productName},#{product.productPrice})
            returning *
            """)
    @ResultMap("resultMapping")
    Product addNewProduct(@Param("product") ProductRequest productRequest);

    @Select("""
            UPDATE product_tb SET product_name = #{product.productName},product_price= #{product.productPrice} 
            where product_id = #{id} 
            returning *
            """)
    @ResultMap("resultMapping")
    Product updateProductById(Integer id,@Param("product") ProductRequest productRequest);

    @Delete("""
            Delete From product_tb where product_id = #{id} 
            """)
    void deleteProductById(Integer id);


    @Select("""
            Select p.product_name,p.product_price from product_tb p
            INNER JOIN invoice_detail_db idt on p.product_id = idt.product_id where idt.invoice_id = #{id}
                """)
    @ResultMap("resultMapping")
    List<Product> getAllProductByInvoiceId(Integer id);
}
