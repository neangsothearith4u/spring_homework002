package com.sothearith.demospringmybatis.repository;

import com.sothearith.demospringmybatis.model.entity.Customer;
import com.sothearith.demospringmybatis.model.entity.Product;
import com.sothearith.demospringmybatis.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

@Mapper
public interface CustomerRepository {


    @Select("""
            SELECT * FROM customer_tb
            """)
    @Results(id = "resultMap",value = {
            @Result(property = "customerId",column = "customer_id"),
            @Result(property = "customerName",column = "customer_name"),
            @Result(property = "customerAddress",column = "customer_address"),
            @Result(property = "customerPhone",column = "customer_phone")
    }
    )
    List<Customer> getAllCustomer();

    @Select("""
            SELECT * FROM customer_tb where customer_id = #{id}
            """)
   @ResultMap("resultMap")
    Customer getCustomerById(Integer id);

    @Select("""
            INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
            values (#{customer.customerName},#{customer.customerAddress},#{customer.customerPhone}) 
            returning *
            """)
    @ResultMap("resultMap")
    Customer insertCustomer(@Param("customer") Customer customer);

        @Update("""
            UPDATE customer_tb SET customer_name = #{customer.customerName},
            customer_address=#{customer.customerAddress},
            customer_phone = #{customer.customerPhone}
            where customer_id = #{id}
            """)
    @ResultMap("resultMap")
    void updateCustomerById(Integer id,@Param("customer") CustomerRequest customerRequest);

        @Delete("""
                DELETE FROM customer_tb where customer_id = #{id}
                """)
    void deleteCustomerById(Integer id);




}
