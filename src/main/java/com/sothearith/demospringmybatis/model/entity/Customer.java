package com.sothearith.demospringmybatis.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Customer {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customerId;
    private String customerName;
    private String customerAddress;
    private Integer customerPhone;
}
