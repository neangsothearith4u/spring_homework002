package com.sothearith.demospringmybatis.model.entity;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer productId;
    private String productName;
    private Float productPrice;
}
