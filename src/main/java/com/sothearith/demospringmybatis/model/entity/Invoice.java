package com.sothearith.demospringmybatis.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private Integer invoiceId;
    private Timestamp invoiceDate;
    private Customer customerId;
    private List<Product> products;
}
