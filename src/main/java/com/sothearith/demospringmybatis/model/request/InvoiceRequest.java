package com.sothearith.demospringmybatis.model.request;

import com.sothearith.demospringmybatis.model.entity.Customer;
import com.sothearith.demospringmybatis.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {
    private Timestamp invoiceDate;
    private Integer customerId;
    private List<Integer> productId;
}
