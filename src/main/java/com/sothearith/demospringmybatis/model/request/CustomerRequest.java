package com.sothearith.demospringmybatis.model.request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CustomerRequest {
    private String customerName;
    private String customerAddress;
    private Integer customerPhone;
}
