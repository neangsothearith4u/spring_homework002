package com.sothearith.demospringmybatis.controller;

import com.sothearith.demospringmybatis.model.entity.Customer;
import com.sothearith.demospringmybatis.model.request.CustomerRequest;
import com.sothearith.demospringmybatis.model.response.ApiResponse;
import com.sothearith.demospringmybatis.service.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer/get-all-customer")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(
                new ApiResponse<List<Customer>>(
                        customerService.getAllCustomer(),
                        "Get all successfully",
                        true
                ));
    }
    @GetMapping("/customer/get-customer-by-id/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer id){
        ApiResponse<Customer> apiResponse = new ApiResponse<Customer>(
                customerService.getCustomerById(id),
                "Get successfully",
                true
        );
        if (apiResponse.getPayload() == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(apiResponse);
    }

    @PostMapping("/customer/add-customer")
    public ResponseEntity<?> addCustomer(@RequestBody Customer customer){
        return ResponseEntity.ok(new ApiResponse<Customer>(
                customerService.insertCustomer(customer),
                "insert successfully",
                true
        ));
    }

    @PutMapping("/customer/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable Integer id,@RequestBody CustomerRequest customerRequest){
         customerService.updateCustomerById(id,customerRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                null,
                "insert successfully",
                true
        ));
    }


    @DeleteMapping("/customer/delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable Integer id){
        customerService.deleteCustomerById(id);
        return ResponseEntity.ok(new ApiResponse<>(
                null,
                "Delete successfully",
                true
        ));
    }

}
