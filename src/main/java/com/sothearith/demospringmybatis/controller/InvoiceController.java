package com.sothearith.demospringmybatis.controller;

import com.sothearith.demospringmybatis.model.entity.Invoice;
import com.sothearith.demospringmybatis.model.request.InvoiceRequest;
import com.sothearith.demospringmybatis.model.response.ApiResponse;
import com.sothearith.demospringmybatis.service.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/Invoice/get-all-invoice")
    public ResponseEntity<?> getAllInvoice(){
        return ResponseEntity.ok(
                new ApiResponse<List<Invoice>>(
                        invoiceService.getAllInvoice(),
                        "Get all invoice successfully",
                        true
                )
        );
    }


    @GetMapping("/Invoice/get-invoice-by-id/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable Integer id){
        return ResponseEntity.ok(
                new ApiResponse<Invoice>(
                        invoiceService.getInvoiceById(id),
                        "Get invoice by id successfully",
                        true
                )
        );
    }

    @PostMapping("/Invoice/add-new-invoice")
    public ResponseEntity<?> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        System.out.println(invoiceRequest);
        invoiceService.addNewInvoice(invoiceRequest);
        return ResponseEntity.ok(
                new ApiResponse<>(
                        null,
                        "Add new invoice successfully",
                        true
                )
        );
    }

    @PutMapping("/Invoice/update-invoice-by-id/{id}")
    public ResponseEntity<?> updateInvoiceById(@PathVariable Integer id,@RequestBody InvoiceRequest invoiceRequest){
        invoiceService.updateInvoiceById(id,invoiceService);
        return ResponseEntity.ok(
                new ApiResponse<>(
                        null,
                        "Update invoice successfully",
                        true
                )
        );
    }


    @DeleteMapping("/Invoice/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable Integer id){
        invoiceService.deleteInvoiceById(id);
        return ResponseEntity.ok(
                new ApiResponse<>(
                        null,
                        "Update invoice successfully",
                        true
                )
        );
    }



}
