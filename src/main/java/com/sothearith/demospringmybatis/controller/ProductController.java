package com.sothearith.demospringmybatis.controller;

import com.sothearith.demospringmybatis.model.entity.Product;
import com.sothearith.demospringmybatis.model.request.ProductRequest;
import com.sothearith.demospringmybatis.model.response.ApiResponse;
import com.sothearith.demospringmybatis.service.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/get-all-product")
    public ResponseEntity<?> getAllProduct(){
        return ResponseEntity.ok(
                new ApiResponse<List<Product>>(
                        productService.getAllProduct(),
                        "Get all product successfully",
                        true
                )
        );
    }


    @GetMapping("/product/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer id){
        return ResponseEntity.ok(
                new ApiResponse<Product>(
                        productService.getProductById(id),
                        "Get product by id successfully",
                        true
                )
        );
    }

    @PostMapping("/product/add-new-product")
    public ResponseEntity <?> addNewProduct(@RequestBody ProductRequest productRequest){
        System.out.println(productRequest.getProductName());
        return ResponseEntity.ok(
                new ApiResponse<Product>(
                        productService.addNewProduct(productRequest),
                        "Add new product successfully",
                        true
                )
        );
    }

    @PutMapping("/product/update-product-by-id/{id}")
    public ResponseEntity<?> updateProductById(@PathVariable Integer id,@RequestBody ProductRequest productRequest){
        return ResponseEntity.ok(
                new ApiResponse<Product>(
                        productService.updateProductById(id,productRequest),
                        "Update product successfully",
                        true
                )
        );
    }

    @DeleteMapping("/product/delete-product-by-id/{id}")
    public ResponseEntity<?> updateProductById(@PathVariable Integer id){
        productService.deleteProductById(id);
        return ResponseEntity.ok(
                new ApiResponse<>(
                        null,
                        "Update product successfully",
                        true
                )
        );
    }
}
