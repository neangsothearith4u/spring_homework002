package com.sothearith.demospringmybatis.service.serviceImp;

import com.sothearith.demospringmybatis.model.entity.Product;
import com.sothearith.demospringmybatis.model.request.ProductRequest;
import com.sothearith.demospringmybatis.repository.ProductRepository;
import com.sothearith.demospringmybatis.service.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product addNewProduct(ProductRequest productRequest) {
        return productRepository.addNewProduct(productRequest);
    }

    @Override
    public Product updateProductById(Integer id, ProductRequest productRequest) {
        return productRepository.updateProductById(id,productRequest);
    }

    @Override
    public void deleteProductById(Integer id) {
        productRepository.deleteProductById(id);
    }


}
