package com.sothearith.demospringmybatis.service.serviceImp;

import com.sothearith.demospringmybatis.model.entity.Customer;
import com.sothearith.demospringmybatis.model.request.CustomerRequest;
import com.sothearith.demospringmybatis.repository.CustomerRepository;
import com.sothearith.demospringmybatis.service.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        return customerRepository.insertCustomer(customer);
    }

    @Override
    public void updateCustomerById(Integer id, CustomerRequest customerRequest) {
        customerRepository.updateCustomerById(id,customerRequest);
    }

    @Override
    public void deleteCustomerById(Integer id) {
        customerRepository.deleteCustomerById(id);
    }


}
