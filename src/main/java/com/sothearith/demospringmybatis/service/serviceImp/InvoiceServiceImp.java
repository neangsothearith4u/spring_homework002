package com.sothearith.demospringmybatis.service.serviceImp;

import com.sothearith.demospringmybatis.model.entity.Invoice;
import com.sothearith.demospringmybatis.model.request.InvoiceRequest;
import com.sothearith.demospringmybatis.repository.InvoiceRepository;
import com.sothearith.demospringmybatis.service.service.InvoiceService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public void addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer invoiceId = invoiceRepository.addNewInvoice(invoiceRequest);
        for (Integer productId : invoiceRequest.getProductId()){
            invoiceRepository.insertIntoInvoiceDetail(invoiceId,productId);
        }
    }

    @Override
    public void updateInvoiceById(Integer id, InvoiceService invoiceService) {
        invoiceRepository.updateInvoice(id,invoiceService);
    }

    @Override
    public void deleteInvoiceById(Integer id) {

    }
}
