package com.sothearith.demospringmybatis.service.service;

import com.sothearith.demospringmybatis.model.entity.Invoice;
import com.sothearith.demospringmybatis.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer id);

    void addNewInvoice(InvoiceRequest invoiceRequest);

    void updateInvoiceById(Integer id, InvoiceService invoiceService);


    void deleteInvoiceById(Integer id);
}
