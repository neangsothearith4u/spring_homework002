package com.sothearith.demospringmybatis.service.service;

import com.sothearith.demospringmybatis.model.entity.Product;
import com.sothearith.demospringmybatis.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();

    Product getProductById(Integer id);


    Product addNewProduct(ProductRequest productRequest);

    Product updateProductById(Integer id, ProductRequest productRequest);

    void deleteProductById(Integer id);
}
