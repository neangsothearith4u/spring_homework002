package com.sothearith.demospringmybatis.service.service;


import com.sothearith.demospringmybatis.model.entity.Customer;
import com.sothearith.demospringmybatis.model.request.CustomerRequest;

import java.util.List;


public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer id);

    Customer insertCustomer(Customer customer);


    void updateCustomerById(Integer id, CustomerRequest customerRequest);

    void deleteCustomerById(Integer id);
}
